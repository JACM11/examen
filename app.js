const express = require('express'),
    path = require ('path'),
    mysql = require('mysql'),
    myConnection = require('express-myconnection');

const app = express();


const clienteRoutes = require('./routes/cliente');


app.set('port', process.env.PORT || 3000);

app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');


app.use(morgan('dev'));
app.use(myConnection(mysql,{
    host:'localhost',
    user:'root',
    password:'',
    port:3306,
    database:'examen'
}));



app.use('/',clienteRoutes);

app.use(express.static(path.join(__dirname,'public')));

app.listen(app.get('port'),()=>{
    console.log('Server on port 3000')
});
