const express = require('express');
const router = express.Router();

const clienteController = require('../controllers/clienteController');
//Metodo post para ingreso de datos
router.post('/nuevo',clienteController.create);

module.exports = router;
